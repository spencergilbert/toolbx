# toolbx

A personalized image for [`toolbox`](https://containertoolbx.org/).

## Usage

Configure `toolbox` to use this custom image by default by creating a conf
file at `.config/containers/toolbox.conf` containing the following:

```toml
[general]
image = "registry.gitlab.com/spencergilbert/toolbx:36"
```

This can also be used ad-hoc by running:

```bash
toolbox create --image registry.gitlab.com/spencergilbert/toolbx:36
```

## Inspiration

[unshippedreminder/fedora-toolbox-custom](https://gitlab.com/unshippedreminder/fedora-toolbox-custom/)

## License

[MIT-0](LICENSE)
