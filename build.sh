#!/usr/bin/env bash

set -o errexit
set -o nounset

registry="${CI_PROJECT_NAME}"
revision="${CI_COMMIT_SHORT_SHA}"
version="${VERSION-latest}"

echo ">> Setting fedora-toolbox:${version} as base image..."
ctr=$(buildah from "docker://registry.fedoraproject.org/fedora-toolbox:${version}")

echo ">> Labeling image..."
buildah config \
  --annotation - \
  --annotation com.github.containers.toolbox="true" \
  --annotation org.opencontainers.image.created="$(date --utc --rfc-3339=seconds)" \
  --annotation org.opencontainers.image.authors="Spencer Gilbert spencer.gilbert@gmail.com" \
  --annotation org.opencontainers.image.url="https://gitlab.com/spencergilbert/toolbx" \
  --annotation org.opencontainers.image.documentation="https://gitlab.com/spencergilbert/toolbx" \
  --annotation org.opencontainers.image.source="https://gitlab.com/spencergilbert/toolbx" \
  --annotation org.opencontainers.image.version="${version}" \
  --annotation org.opencontainers.image.revision="${revision}" \
  --annotation org.opencontainers.image.vendor="Spencer Gilbert spencer.gilbert@gmail.com" \
  --annotation org.opencontainers.image.licenses="CC0" \
  --annotation org.opencontainers.image.ref.name="" \
  --annotation org.opencontainers.image.title="toolbx" \
  --annotation org.opencontainers.image.description="Personal Container Toolbx Image" \
  --env NAME=toolbx \
  --label - \
  --label com.github.containers.toolbox="true" \
  --label org.opencontainers.image.created="$(date --utc --rfc-3339=seconds)" \
  --label org.opencontainers.image.authors="Spencer Gilbert spencer.gilbert@gmail.com" \
  --label org.opencontainers.image.url="https://gitlab.com/spencergilbert/toolbx" \
  --label org.opencontainers.image.documentation="https://gitlab.com/spencergilbert/toolbx" \
  --label org.opencontainers.image.source="https://gitlab.com/spencergilbert/toolbx" \
  --label org.opencontainers.image.version="${version}" \
  --label org.opencontainers.image.revision="${revision}" \
  --label org.opencontainers.image.vendor="Spencer Gilbert spencer.gilbert@gmail.com" \
  --label org.opencontainers.image.licenses="CC0" \
  --label org.opencontainers.image.ref.name="" \
  --label org.opencontainers.image.title="toolbx" \
  --label org.opencontainers.image.description="Personal Container Toolbx Image" \
  "${ctr}"

echo ">> Installing packages..."
buildah run "${ctr}" bash -c "dnf update --assumeyes; \
  dnf install --assumeyes \
  bat \
  dua-cli \
  git-delta \
  fd-find \
  fzf \
  jq \
  lsd \
  neovim \
  ripgrep \
  ShellCheck \
  shfmt \
  starship \
  zoxide; \
  dnf clean all"

echo ">> Finishing up..."
buildah commit "${ctr}" "${registry}:${version}"
