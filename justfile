#!/usr/bin/env just --justfile

_default:
    @just --list

fmt:
    shfmt -f . | xargs -I{} shfmt -i 2 -ci -w {}

lint:
    shfmt -f . | xargs -I{} shellcheck -x {}

toolbx-build version='latest' project='spencergilbert/toolbx' revision=`git rev-parse --short HEAD`:
    flatpak-spawn --host --env=CI_COMMIT_SHORT_SHA={{ revision }} \
    	--env=CI_PROJECT_NAME={{ project }} \
    	--env=VERSION={{ version }} \
    	./build.sh

toolbx-list label='com.github.containers.toolbox':
    flatpak-spawn --host buildah images --filter label={{ label }}
